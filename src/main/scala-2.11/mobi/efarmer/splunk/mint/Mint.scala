package mobi.efarmer.splunk.mint

import java.util.UUID
import java.util.logging.Logger

/**
 * @author Maxim Maximchuk
 *         date 09.09.2015.
 */
case class Credential(apiKey: String, token: String)

case class AppInfo(packageName: String, appName: String, appVersion: String) {
  val sdkName = "scala-mint-sdk"
  val sdkVersion = "1.3.3"
}

object Mint {

  private val logger = Logger.getLogger(getClass.getSimpleName)

  private var credentialOption = None: Option[Credential]
  private var appInfoOption = None: Option[AppInfo]
  private var uuidOption = None: Option[String]

  def init(credential: Credential, appInfo: AppInfo): Unit = {
    credentialOption = Option(credential)
    appInfoOption = Option(appInfo)
    uuidOption = Some(UUID.randomUUID().toString)
  }

  def prepareRequest(): MintSender = {
    sender()
  }

  def send(throwable: Throwable, handled: Boolean = false): Unit = {
    sender().send(throwable, handled)
  }

  private def sender(): MintSender = {
    (uuidOption, credentialOption, appInfoOption) match {
      case (Some(uuid), Some(credential), Some(appInfo)) => new InitedMintSender(credential, appInfo, uuid)
      case (_, None, _) =>
        logger.severe("Credential isn`t defined")
        new NullMintSender
      case (_, _, None) =>
        logger.severe("AppInfo isn`t defined")
        new NullMintSender
      case _ => new NullMintSender
    }
  }

}
