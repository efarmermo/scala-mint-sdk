package mobi.efarmer.splunk.mint

import java.io.{ByteArrayOutputStream, PrintStream}
import java.util
import java.util.logging.Logger

import com.maximchuk.rest.api.client.content.JsonRestApiContent
import com.maximchuk.rest.api.client.core.{RestApiMethod, DefaultClient}
import com.maximchuk.rest.api.client.http.HttpHeader
import mobi.efarmer.splunk.mint.MintJsonProtocol._
import spray.json._

import scala.collection.JavaConversions._
import scala.collection.immutable.HashMap
import scala.collection.{immutable, mutable}

/**
  * @author Maxim Maximchuk
  *         date 09.09.2015.
  */
case class MintJsonDto(uuid: String, appInfo: AppInfo, exceptionJsonDto: ExceptionJsonDto, requestJsonDto: RequestJsonDto)

case class RequestJsonDto(handled: Boolean, customDataJsonDtoOption: Option[CustomDataJsonDto] = None)

case class CustomDataJsonDto(extras: immutable.Map[String, String])

case class ExceptionJsonDto(message: String, where: String, klass: String, backtrace: String)

trait MintSender {
  protected val logger = Logger.getLogger(getClass.getSimpleName)

  val extrasMap = new mutable.HashMap[String, String]

  def addExtraData(key: String, value: String): MintSender = {
    if (value != null) {
      extrasMap += (key -> value)
    }
    this
  }

  def addExtraDataMap(extras: mutable.Map[String, String]): MintSender = {
    extrasMap ++= extras
    this
  }

  def addExtraDataMap(extras: util.Map[String, String]): MintSender = {
    extras.entrySet().foreach(i => extrasMap += (i.getKey -> i.getValue))
    this
  }

  def send(throwable: Throwable, handled: Boolean = false): Unit
}

class InitedMintSender private[mint](credential: Credential, appInfo: AppInfo, uuid: String) extends MintSender {

  override def send(throwable: Throwable, handled: Boolean) = {
    try {
      val os = new ByteArrayOutputStream()
      throwable.printStackTrace(new PrintStream(os))
      val element = throwable.getStackTrace.apply(0)
      val exceptionJsonDto = ExceptionJsonDto(if (throwable.getMessage != null) throwable.getMessage else "null", element.getLineNumber.toString, element.getClassName, new String(os.toByteArray))

      var customDataJsonDtoOption = None: Option[CustomDataJsonDto]

      if (extrasMap.nonEmpty) {
        customDataJsonDtoOption = Some(CustomDataJsonDto(new HashMap[String, String] ++ extrasMap))
      }

      val requestJsonDto = RequestJsonDto(handled, customDataJsonDtoOption)

      val mintJson = MintJsonDto(uuid, appInfo, exceptionJsonDto, requestJsonDto).toJson

      val client = new DefaultClient("https://mint.splunk.com/api", "errors")
      val method = new RestApiMethod(RestApiMethod.Type.POST)
      method.addHeader(new HttpHeader("X-BugSense-Api-Key", credential.apiKey))
      method.addHeader(new HttpHeader("X-Splunk-Mint-Auth-Token", credential.token))
      method.setContent(JsonRestApiContent.create(mintJson.toString()))
      val response = client.execute(method)

      val respJson = response.getString.parseJson.asJsObject
      respJson.fields.apply("error") match {
        case err: JsString => logger.severe(s"sending error with message '${err.value}'")
        case _ => //ignoring
      }
    } catch {
      case e: Exception => logger.severe(e.getMessage)
    }
  }
}

class NullMintSender private[mint] extends MintSender {
  override def send(throwable: Throwable, handled: Boolean) = {
    logger.severe("Mint SDK is not initialized properly. You need to call Mint init first")
  }
}


