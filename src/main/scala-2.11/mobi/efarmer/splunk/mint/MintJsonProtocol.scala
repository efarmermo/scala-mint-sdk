package mobi.efarmer.splunk.mint

import spray.json._

/**
 * @author Maxim Maximchuk
 *         date 10-Sep-15.
 */
object MintJsonProtocol extends DefaultJsonProtocol {

  implicit def exceptionJsonFormat = jsonFormat4(ExceptionJsonDto.apply)

  implicit object CustomDataJsonFormat extends RootJsonFormat[CustomDataJsonDto] {
    override def write(obj: CustomDataJsonDto): JsValue = {
      obj.extras.toJson
    }

    override def read(json: JsValue): CustomDataJsonDto = ???
  }

  implicit object RequestJsonFormat extends RootJsonFormat[RequestJsonDto] {
    override def write(obj: RequestJsonDto): JsValue = {
      JsObject(
        "handled" -> JsNumber(if (obj.handled) 1 else 0),
        "custom_data" -> customDataJson(obj)
      )
    }

    override def read(json: JsValue): RequestJsonDto = ???

    private def customDataJson(requestJsonDto: RequestJsonDto) = {
      requestJsonDto.customDataJsonDtoOption match {
        case Some(r) => r.toJson
        case None => JsObject()
      }
    }
  }

  implicit object MintJsonFormat extends RootJsonFormat[MintJsonDto] {
    def write(obj: MintJsonDto) = {
      JsObject(
        "client" -> JsObject("name" -> JsString(obj.appInfo.sdkName), "version" -> JsString(obj.appInfo.sdkVersion)),
        "request" -> obj.requestJsonDto.toJson,
        "exception" -> obj.exceptionJsonDto.toJson,
        "application_environment" -> JsObject(
          "phone" -> JsString(obj.appInfo.appName),
          "appver" -> JsString(obj.appInfo.appVersion),
          "appname" -> JsString(obj.appInfo.packageName),
          "osver" -> JsString(obj.appInfo.appVersion),
          "uid" -> JsString(obj.uuid)
        )
      )
    }

    def read(json: JsValue) = ???
  }

}
