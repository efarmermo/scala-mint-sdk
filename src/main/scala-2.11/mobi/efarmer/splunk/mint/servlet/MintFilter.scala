package mobi.efarmer.splunk.mint.servlet

import java.io.{File, FileInputStream}
import java.util.jar.Manifest
import java.util.logging.Logger
import javax.servlet._

import mobi.efarmer.splunk.mint.{AppInfo, Credential, Mint}

/**
 * @author Maxim Maximchuk
 *         date 22-Sep-15.
 */
class MintFilter extends Filter {

  private val logger = Logger.getLogger(getClass.getSimpleName)

  override def init(filterConfig: FilterConfig): Unit = {
    try {
      val manifestPathOpt = Option(filterConfig.getInitParameter("splunk.mint.manifestPath"))
      val apiKeyOpt = Option(filterConfig.getInitParameter("splunk.mint.apiKey"))
      val tokenOpt = Option(filterConfig.getInitParameter("splunk.mint.token"))

      // read manifest
      val servletContext = filterConfig.getServletContext
      val is = manifestPathOpt match {
        case Some(manifestPath) => new FileInputStream(new File(new File(servletContext.getRealPath("/")), manifestPath))
        case None => new FileInputStream(new File(new File(servletContext.getRealPath("/")), "META-INF/MANIFEST.MF"))
      }

      val attrs = new Manifest(is).getMainAttributes

      val title = Option(attrs.getValue("Implementation-Title")) match {
        case Some(t) => t
        case None =>
          logger.warning("Implementation-Title doesn`t specified in Manifest")
          ""
      }
      val version = Option(attrs.getValue("Implementation-Version")) match {
        case Some(v) => v
        case None =>
          logger.warning("Implementation-Version doesn`t specified in Manifest")
          "1.0"
      }
      val vendor = Option(attrs.getValue("Implementation-Vendor-Id")) match {
        case Some(v) => v
        case None =>
          logger.warning("Implementation-Vendor-Id doesn`t specified in Manifest")
          ""
      }

      (apiKeyOpt, tokenOpt) match {
        case (Some(apiKey), Some(token)) =>
          Mint.init(Credential(apiKey, token), AppInfo(vendor, title, version))
        case (None, _) => logger.severe("splunk.mint.apiKey param doesn`t specified")
        case (_, None) => logger.severe("splunk.mint.token param doesn`t specified")
      }

    } catch {
      case e: Exception => logger.severe(e.getMessage)
    }

  }

  override def doFilter(servletRequest: ServletRequest, servletResponse: ServletResponse, filterChain: FilterChain): Unit = {
    try {
      filterChain.doFilter(servletRequest, servletResponse)
    } catch {
      case th: Throwable =>
        Mint.send(th)
        throw th
    }
  }

  override def destroy(): Unit = {}

}
