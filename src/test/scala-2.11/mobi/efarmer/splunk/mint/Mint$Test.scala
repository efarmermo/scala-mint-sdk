package mobi.efarmer.splunk.mint

import java.util

import org.scalatest.FunSuite

import scala.collection.mutable

/**
 * @author Maxim Maximchuk
 *         date 09.09.2015.
 */
class Mint$Test extends FunSuite {

  val credential = Credential("a4e08f67", "5f13684c753b7996279a4d4")
  val appInfo = AppInfo("mobi.efarmer.splunk.mint", "mint-sdk", "1.3")

  test("testInit") {
    Mint.init(credential, appInfo)
  }

  test("testPrepareRequest") {
    Mint.init(credential, appInfo)
    val map = mutable.HashMap("MyParam3" -> "map", "MyParam4" -> "map")
    val javaMap = new util.HashMap[String, String]()
    javaMap.put("MyParam5", "javaMap")
    javaMap.put("MyParam6", "javaMap")
    try {
      try {
        wrapException("testPrepareRequest")
      } catch {
        case e: TestException =>
          Mint.prepareRequest()
            .addExtraData("MyParam1", "value")
            .addExtraData("MyParam2", "value")
            .addExtraDataMap(map)
            .addExtraDataMap(javaMap)
            .send(e, handled = true)
      }
    } catch {
      case e: Exception => fail(e)
    }
  }

  test("send") {
    try {
      try {
        Mint.init(credential, appInfo)
        wrapException("send")
      } catch {
        case e: TestException => Mint.send(e, handled = true)
      }
    } catch {
      case e: Exception => fail(e)
    }
  }

  private def throwException(msg: String) = {
    throw new TestException(s"Test '$msg': throwException method")
  }

  private def wrapException(msg: String) = {
    try {
      throwException(msg)
    } catch {
      case e: TestException => throw new TestException(s"Test '$msg': wrapException method", e)
    }
  }

}

class TestException(msg: String, e: Throwable = null) extends Exception(msg, e)
