name := "mint-sdk"

organization := "mobi.efarmer.splunk.mint"

version := "1.3.4"

scalaVersion := "2.11.7"

val repository = "eFarmer repository" at "http://dev.efarmer.mobi:8889/repository/internal"

resolvers += repository

publishTo := Some(repository)

credentials += Credentials(Path.userHome / ".sbt" / ".archivaCredentials")

libraryDependencies ++= Seq(
  "io.spray" %% "spray-json" % "1.3.2",
  "com.maximchuk.rest" % "api-client" % "0.13",
  "javax" % "javaee-web-api" % "6.0",

  "org.scalatest" %% "scalatest" % "2.2.4" % "test"
)

dependencyOverrides += "org.scala-lang" % "scala-reflect" % "2.11.7"
dependencyOverrides += "org.scala-lang.modules" %% "scala-xml" % "1.0.4"
